<?php 

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
require_once('db_lib.php');
$oDB = new db;
$result = $oDB->select('SELECT * FROM portfolio ORDER BY `portfolio`.`portfolio_id` ASC');

$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
	if ($outp != "") {$outp .= ",";}
	$outp .= '{"portfolio_id":"'  . $rs["portfolio_id"] . '",';
    $outp .= '"portfolio_title":"'   . $rs["portfolio_title"]        . '",';
	$outp .= '"portfolio_tag_Line":"'   . $rs["portfolio_tag_Line"]        . '",';
	$outp .= '"portfolio_url":"'   . $rs["portfolio_url"]        . '",';
	$outp .= '"portfolio_background_url":"'   . $rs["portfolio_background_url"]        . '",';
	$outp .= '"portfolio_fullscreen_url":"'. $rs["portfolio_fullscreen_url"]     . '"}';   
}


   
$outp ='{"Portfolio":['.$outp.']}';

echo ($outp);
?>
  