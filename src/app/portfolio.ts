export class Portfolio {
  portfolio_id: number;
  portfolio_title: string;
  portfolio_tag_Line: string;
  portfolio_url: string;
  portfolio_background_url: string;
  portfolio_fullscreen_url: string;
}