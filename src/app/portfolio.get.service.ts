import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Portfolio } from './portfolio';
 
@Injectable()
export class PortfolioGetService {
 
  private headers = new Headers({'Content-Type': 'application/json'});
  private portfolioUrl = 'http://localhost/pristinit/getPortfolioGrid.php';  // URL to web api
 
  constructor(private http: Http) { }
 
 /* To fetch all portfolios response */
  getPortfolios(): Promise<Portfolio[]> {
	  
		return this.http.get(this.portfolioUrl)
			   .toPromise()
			   .then(response => response.json() as Portfolio[]) //
			   .catch(this.handleError);
  }
 
 /* To fetch specific portfolio details */
  getPortfolio(id: number): Promise<Portfolio[]> {
	const url = `${this.portfolioUrl}/${id}`;
	return this.http.get(url)
	  .toPromise()
	  .then(response => response.json() as Portfolio[])
	  .catch(this.handleError);
  }
 
 /* To delete specific portfolio from database */
  delete(id: number): Promise<void> {
	const url = `${this.portfolioUrl}/${id}`;
	return this.http.delete(url, {headers: this.headers})
	  .toPromise()
	  .then(() => null)
	  .catch(this.handleError);
  }
 
 /* To create a portfolio with its details */
  create(portfolio: Portfolio): Promise<Portfolio> {
	return this.http
	  .post(this.portfolioUrl, JSON.stringify(portfolio), {headers: this.headers})
	  .toPromise()
	  .then(res => res.json() as Portfolio)
	  .catch(this.handleError);
  }
 
 /* To update specific portfolio details*/
  update(portfolio: Portfolio): Promise<Portfolio> {
	const url = `${this.portfolioUrl}/${portfolio.portfolio_id}`;
	return this.http
	  .put(url, JSON.stringify(portfolio), {headers: this.headers})
	  .toPromise()
	  .then(response => response.json() as Portfolio)
	  .catch(this.handleError);
  }
  
  

 /* Handler to catch any http errors */
  private handleError(error: any): Promise<any> {
	console.error('An error occurred', error); // for demo purposes only
	return Promise.reject(error.message || error);
  }
}