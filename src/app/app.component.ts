import { Component } from '@angular/core';

@Component({
  selector: 'pristinit',
  template: `<app-header></app-header> <router-outlet></router-outlet>`
})
export class AppComponent  { }
