import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './views/header.html',
  styleUrls: ['./css/header.css'] 
  
})

export class HeaderComponent implements OnInit {
	active = 0;	
	mySplitText = '';
	t1 = new TimelineMax();
	t2 = new TimelineMax({paused:true, reversed: true})
	i = 6;
	shapes = '';
	
	
    
  constructor() {
    
  }

  ngOnInit() {
			
			this.mySplitText = new SplitText(".txt", {type:"words,chars"});
			console.log(this.mySplitText.chars);
			this.t2.to(".line3",0, {stroke:"#000"} )
			.to(".line1" , 0, {stroke:"#000"})
			.to(".line2" , 0, {stroke:"#000"});
			this.t2.to(".menu_txt" , 0, {x:55, fill:"#000"});
			this.t2.to(".txt" , 0, {x:10, y:-4})
			.to(".menu_txt" , 0, {fill:"#fff"})
			.to(".menu_txt" , 0.1, {alpha:0, x:45} , "+=0.5")
			.to(".line3",0, {stroke:"#ffffff"})
			.to(".line1" , 0, {stroke:"#ffffff"})
			.to(".line2" , 0, {stroke:"#ffffff"})
			.to(".line", 0.8, {rotationZ:-90, transformOrigin:"center center"})
			
			.to(".line3",0.8, {y:10, stroke:"#ffffff"} , "+=0")
			.to(".line1" , 0.8, {rotationZ:45, transformOrigin:"center", stroke:"#ffffff"}, "-=0.8")
			.to(".line2" , 0.8, {rotationZ:-45, y:-6, transformOrigin:"center", stroke:"#ffffff"} , "-=0.8")
			
			.staggerFromTo(".line3", 0.1, {autoAplha:0},{autoAlpha: 1,  y:"+="+11, stroke:"#ffffff", ease: SteppedEase.config(1)})
			.staggerFromTo(".line3", 0.1, {autoAplha:0},{autoAlpha: 1,  y:"+="+3, stroke:"#ffffff",  ease: SteppedEase.config(1)})
			.staggerFromTo(".line3", 0.1, {autoAplha:0},{autoAlpha: 1,  y:"+="+3, stroke:"#ffffff", ease: SteppedEase.config(1)})
			.staggerFromTo(".line3", 0.1, {autoAplha:0},{autoAlpha: 1,  y:"+="+6, stroke:"#ffffff", ease: SteppedEase.config(1)})
			.staggerFromTo(".line3", 0.1, {autoAplha:0},{autoAlpha: 1,  y:"+="+6, stroke:"#ffffff", ease: SteppedEase.config(1)})
			.staggerFrom(this.mySplitText.chars,0.5, {opacity:0, ease:Power1.easeIn, fill:"#ffffff"}, 0.1 , "-=0.5")
			.to(".line3",0, {alpha:0}); 
			
			
	
  }
  
  displayMenu(event: any): void {
					
			
			
			if(this.active == 1)
			{
			this.t1.to(".menu-link",0, {'position':'absolute'});
			this.active = 0;
			this.t2.timeScale(6);
			this.t2.reverse();
			this.t1.staggerFromTo(".menu-wrapper", 0.8, {y: "0px", ease: Expo.easeOut, display:'block' },{y: "-" +percentToPixel(120) + "px", ease: Expo.easeOut }, "1");

			}
			else if(this.active == 0)
			{
			
			this.active = 1;
			this.t1.to(".menu-link",0, {'position':'fixed'});
			this.t2.timeScale(3);
			this.t2.restart();
			this.t1.staggerFromTo(".menu-wrapper", 0.8, {y: "-" +percentToPixel(120) + "px", ease: Expo.easeOut, display:'block' },{y: "-50px", ease: Expo.easeOut });

			}
			function percentToPixel(_perc){
			  return ($(window).height()/100)* parseFloat(_perc);
			} 
	}
	
	changepage(event: any): void{
		
		console.log('active == 1');
			this.active = 0;
			this.t2.timeScale(6);
			this.t2.reverse();
			this.t1.staggerFromTo(".menu-wrapper", 0.8, {y: "0px", ease: Expo.easeOut, display:'block' },{y: "-" +percentToPixel(120) + "px", ease: Expo.easeOut }, "1");
		 setTimeout(() => {
			
		  }
		  , 1000);
		 
		 function percentToPixel(_perc){
			  return ($(window).height()/100)* parseFloat(_perc);
			}
	}
}
