import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'pristinit',
  templateUrl: `./views/mobile-development.html`,
  styleUrls: [ './css/mobile-development.css']
})
export class mobiledevelopmentComponent  { 
	
	mainOptions = {
		sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE'],
		navigation: true,
		navigationPosition: 'right',
		scrollingSpeed: 1000
	}
	ngOnInit() {
		//$('#fullpage').fullpage();
	}
}