import { Component } from '@angular/core';

@Component({
  selector: 'pristinit',
  templateUrl: `./views/contact-us.html`,
  styleUrls: [ './css/contact-us.css' ]
})
export class ContactUsComponent  { name = 'Contact Us!'; }
