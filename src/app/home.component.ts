import { Component, OnInit,ViewChild } from '@angular/core';
import { footer_oneComponent } from './footer_one.component';



@Component({
  selector: 'pristinit',
  templateUrl: `./views/home.html`,
  styleUrls: [ './css/home.css' ]
})
export class HomeComponent  { 
	@ViewChild(footer_oneComponent) footer: footer_oneComponent;
	ngOnInit() {
		
		//for footer change
		this.footer.changeColor('footerGreen')
	}
}
