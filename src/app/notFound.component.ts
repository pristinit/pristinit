import { Component } from '@angular/core';

@Component({
  selector: 'pristinit',
  templateUrl: `./views/notFound.html`,
  styleUrls: [ './css/notFound.css' ]
})
export class notFoundComponent  { name = 'notFound'; }
