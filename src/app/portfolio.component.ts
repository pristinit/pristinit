import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Portfolio } from './portfolio';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { PortfolioGetService } from './portfolio.get.service';



@Component({
  selector: 'pristinit',
  templateUrl: `./views/portfolio.html`,
  styleUrls: [ './css/portfolio.css' ]
})

export class PortFolioComponent implements OnInit, AfterViewInit {
    name = 'Work';
	result:Portfolio[] = [];
	portfolioCurrentFullScreenImage = "";
    workCellImageWrapperOpacity = 1;
    fullScreenOpacity = 0;
    windowHeight = 0;
    windowWidth = 0;
    workWrapperHeight = 0;
    workWrapperWidth = 0;
    workCellHeight = 0;
    workCellWidth = 0;
    workCellClass = "";
    scrollOffset = 0;
    cellClass = "";
    currentCellId = 0;
    workCellTitleOpacity = 1;
    workCellTagLineOpacity = 1;
    mouseInsideWrapper = 0;
    mouseScrolled = 0;


    constructor(
    private portfolioService: PortfolioGetService) { }


    ngOnInit() {

        this.portfolioService
            .getPortfolios()
            .then(result => this.result = result);  
        this.windowHeight =  $(window).height();
        this.windowWidth = $(window).width();
        this.workWrapperHeight = $(window).height() - 130;
        this.workWrapperWidth = $(window).width() - 100;
        this.workCellHeight = (this.workWrapperHeight - 20) / 3;
        this.workCellWidth = (this.workWrapperWidth - 20) / 3;
        console.log(this.workWrapperHeight + " " + this.windowWidth);
        //TweenMax.to(".fullscreen-image", 0.2	, {scale: 1.03});
    }

    ngAfterViewInit() {
        //TweenMax.to(".work-cell",0.001, {x: 50});
        //TweenMax.staggerFromTo(".work-cell", 0.5, {autoAlpha: 0}, {autoAlpha: 1, x: 0}, 0.25);
        ////TweenMax.to(element, 2, {autoAlpha:1, delay:2});
    }

    resizeEvent(): void {
        this.windowHeight =  $(window).height();
        this.windowWidth = $(window).width();
        this.workWrapperHeight = $(window).height() - 130;
        this.workWrapperWidth = $(window).width() - 100;
        this.workCellHeight = (this.workWrapperHeight - 20) / 3;
        this.workCellWidth = (this.workWrapperWidth - 20) / 3;
    }


    displayGrid(): void {
        console.log("displayGrid");
		//TweenMax.to(".fullscreen-image", 0, {scale: 1.1});
        //TweenMax.killTweensOf(".work-cell-animate");
        //TweenMax.killTweensOf(".fullscreen-image");
        //TweenMax.killTweensOf(".work-cell-tag-line-animate");
        //TweenMax.killTweensOf(".work-cell-title-animate");
        this.workCellImageWrapperOpacity = 1;
        this.fullScreenOpacity = 0;
        $("#cell-" + this.currentCellId).addClass("work-cell-animate");
        $("#title-" + this.currentCellId).addClass("work-cell-title-animate");
        $("#tag-line-" + this.currentCellId).addClass("work-cell-tag-line-animate");
        //TweenMax.to("#tag-line-" + this.currentCellId, 0.2, {y: 0, alpha: 0,});
        //TweenMax.to("#title-" + this.currentCellId, 0.2, {y: 0, alpha: 0.5, delay: 0.2});
        //TweenMax.to(".fullscreen-image", 5, {scale: 1});
    }

    displayFullsreen(cur: Portfolio[], id: number): void {
				//TweenMax.to(".fullscreen-image", 0, {scale: 1.1});
                //TweenMax.killTweensOf(".work-cell-animate");
                //TweenMax.killTweensOf(".fullscreen-image");
                //TweenMax.killTweensOf(".work-cell-tag-line-animate");
                //TweenMax.killTweensOf(".work-cell-title-animate");
                this.workCellTitleOpacity = 1;
                this.workCellTagLineOpacity = 1;
                // console.log("displayfullscreen called");
                // console.log(event);
                this.currentCellId = id;
                $("#cell-" + id).removeClass("work-cell-animate");
                $("#title-" + id).removeClass("work-cell-title-animate");
                $("#tag-line-" + id).removeClass("work-cell-tag-line-animate");
				console.log(id);
				//console.log(this.result.Portfolio);
				
				//this.portfolioCurrentFullScreenImage = this.result.Portfolio[id-1].portfolio_fullscreen_url;
				//TweenMax.to(".work-cell-image-wrapper", 0.5, {alpha: 1});
                //TweenMax.to(".work-cell-animate", 0.5, {alpha: 0, delay: 0.2});
                //TweenMax.to(".fullscreen-image", 0.2, {alpha: 1 });
                //TweenMax.to("#tag-line-" + id, 0.3, {y: 0, alpha: 1, delay: 0.2});
                //TweenMax.to("#title-" + id, 0.5, {y: 0, alpha: 1, delay: 0.3});
                //TweenMax.to(".work-cell-title-animate", 0.5, {y: (this.workCellHeight/100)*(parseFloat(5)), alpha: 0.4, delay: 0.3});				
                //TweenMax.to(".work-cell-tag-line-animate", 0.3, {y: 14, alpha: 0, delay: 0.2});
				//TweenMax.to(".fullscreen-image", 5, {scale: 1});
                
				

    }
	

	

    displayWrapperGrid(): void {
        console.log("out of section");
        this.mouseInsideWrapper = 0;
        //TweenMax.killTweensOf(".work-cell-animate");
        //TweenMax.killTweensOf(".fullscreen-image");
        //TweenMax.killTweensOf(".work-cell-tag-line-animate");
        //TweenMax.killTweensOf(".work-cell-title-animate");
        this.workCellImageWrapperOpacity = 1;
        this.fullScreenOpacity = 0;
        $("#cell-" + this.currentCellId).addClass("work-cell-animate");
        $("#title-" + this.currentCellId).addClass("work-cell-title-animate");
        $("#tag-line-" + this.currentCellId).addClass("work-cell-tag-line-animate");
        //this.timeLineMax.play();
        //TweenMax.to(".fullscreen-image", 0.2, {alpha: 0});
        //TweenMax.to(".work-cell-animate", 0.2, {alpha: 1});
        //TweenMax.to(".work-cell-tag-line-animate", 0.2, {y: 0, alpha: 1});
        //TweenMax.to(".work-cell-title-animate", 0.2, {y: 0, alpha: 1, delay: 0.2});
        //TweenMax.to(".fullscreen-image", 0.2, {scale: 1.03});
    }



    onScrollUp(event: any): void {
        this.mouseScrolled = 1;
        this.displayGrid();
        console.log("scroll Up called");
        this.scrollOffset = this.scrollOffset - this.workCellHeight - 10;
        if(this.scrollOffset < 0) {
            this.scrollOffset = 0;
        }
        console.log(this.scrollOffset);
		console.log(this.workCellHeight);
        console.log(this.scrollOffset);
        //TweenMax.to(".fullscreen-image", 0.2, {alpha: 0});
        //TweenMax.to(".work-cell-animate", 0.2, {alpha: 1, delay: 0.2});
        //TweenMax.to(".work-cell-tag-line-animate", 0.2, {y: 0, alpha: 1});
        //TweenMax.to(".work-cell-title-animate", 0.2, {y: 0, alpha: 1});
        //TweenMax.to("#work-wrapper", 1.5, {scrollTo: {y: this.scrollOffset}, ease:Power4.easeOut});
    }

    onScrollDown(event: any): void {
        this.mouseScrolled = 1;
        this.displayGrid();
        
        this.scrollOffset = this.scrollOffset + this.workCellHeight + 10;
        if(this.scrollOffset > (this.workCellHeight * 7) + 70) {
            this.scrollOffset = (this.workCellHeight * 7) + 70;
			console.log("scroll Down called");
        }
        console.log(this.workCellHeight);
        console.log(this.scrollOffset);
        //TweenMax.to(".fullscreen-image", 0.2, {alpha: 0});
        //TweenMax.to(".work-cell-animate", 0.2, {alpha: 1, delay: 0.2});
        //TweenMax.to(".work-cell-tag-line-animate", 0.2, {y: 0, alpha: 1});
        //TweenMax.to(".work-cell-title-animate", 0.2, {y: 0, alpha: 1});
        //TweenMax.to("#work-wrapper", 1.5, {scrollTo: {y: this.scrollOffset}, ease:Power4.easeOut});
    }

    insideWrapper(): void {
        this.mouseInsideWrapper = 1;
    }
}
