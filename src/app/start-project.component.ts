import { Component,OnInit} from '@angular/core';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';

@Component({
  selector: 'pristinit',
  templateUrl: `./views/start-project.html`,
  styleUrls: [ './css/start-project.css' ],
   animations: [
    trigger(
    'animationIf', [
		transition(':enter', [
          style({opacity: 0}),
          animate('500ms', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('0s', style({opacity: 0}))
        ])
      ]
    )
  ],
})


export class startprojectComponent implements OnInit  { 
	 
	step1Show = true;
	step2Show = false;
	step3Show = false;
	step4Show = false;
	step5Show = false;
	
	
		
		
	ngOnInit() {
		var handle = $('#slider2 A.ui-slider-handle');        
		handle.eq(0).addClass('first-handle');        
		handle.eq(1).addClass('second-handle');
	}
	
	step_one(): void {
		
		this.step1Show = true;
		this.step2Show = false;			
	}
	step_two(): void {
		this.step1Show = false;		
		this.step2Show = true;	
		this.step3Show = false;	
	}
	step_three(): void {
		this.step2Show = false;		
		this.step3Show = true;	
		this.step4Show = false;	
	}
	step_four(): void {
		
		this.step3Show = false;		
		this.step4Show = true;	
		this.step5Show = false;
	}
	step_five(): void {
		this.step4Show = false;		
		this.step5Show = true;			
	}
}


