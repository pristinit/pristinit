import { NgModule }      from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent }  from './app.component';
import { HomeComponent }  from './home.component';
import { PortFolioComponent }  from './portfolio.component';
import { AboutUsComponent }  from './about-us.component';
import { ContactUsComponent }  from './contact-us.component';
import { notFoundComponent }  from './notFound.component';
import { blogComponent }  from './blog.component';
import { blogsingleComponent }  from './blog-single.component';
import { brandingComponent }  from './branding.component';
import { careerComponent }  from './career.component';
import { mobiledevelopmentComponent }  from './mobile-development.component';
import { promotionalComponent }  from './promotional.component';
import { servicesComponent }  from './services.component';
import { startprojectComponent }  from './start-project.component';
import { webdevelopmentComponent }  from './web-development.component';
import { worksingleaaraComponent }  from './work-single-aara.component';



const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'work', component: PortFolioComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: '404', component: notFoundComponent },
  { path: 'blog', component: blogComponent },
  { path: 'blog-single', component: blogsingleComponent},
  { path: 'branding', component: brandingComponent},
  { path: 'career', component: careerComponent},
  { path: 'mobile-development', component: mobiledevelopmentComponent},
  { path: 'promotional', component: promotionalComponent},
  { path: 'services', component: servicesComponent},
  { path: 'start-project', component: startprojectComponent},
  { path: 'web-development', component: webdevelopmentComponent },
  { path: 'work-single-aara', component: worksingleaaraComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class PristinitRoutingModule { }
