import { Directive, Output, HostListener, EventEmitter } from '@angular/core';

@Directive({ selector: '[mouseWheel]' })
export class MouseWheelDirective {
  @Output() mouseWheelUp = new EventEmitter();
  @Output() mouseWheelDown = new EventEmitter();
  prevTime = 0;


  @HostListener('mousewheel', ['$event']) onMouseWheelChrome(event: any) {
    this.mouseWheelFuncWrapper(event);
  }

  @HostListener('DOMMouseScroll', ['$event']) onMouseWheelFirefox(event: any) {
    this.mouseWheelFuncWrapper(event);
  }

  @HostListener('onmousewheel', ['$event']) onMouseWheelIE(event: any) {
    this.mouseWheelFuncWrapper(event);
  }

  mouseWheelFuncWrapper(event: any) {
    var timeStamp = Math.floor(Date.now());
    // console.log("prevTime: " + this.prevTime);
    // console.log("timeStamp :" + timeStamp);
    // console.log(timeStamp - this.prevTime);
    if(this.prevTime == 0) {
      this.mouseWheelFunc(event);
      this.prevTime = timeStamp;
    }
    else {
      if((timeStamp - this.prevTime) > 400 ) {
        this.mouseWheelFunc(event);
        this.prevTime = timeStamp;
      }
    }
  }

  mouseWheelFunc(event: any) {
    var event = window.event || event; // old IE support
    var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
    if(delta > 0) {
        this.mouseWheelUp.emit(event);
    } else if(delta < 0) {
        this.mouseWheelDown.emit(event);
    }
    // for IE
    event.returnValue = false;
    // for Chrome and Firefox
    if(event.preventDefault) {
        event.preventDefault();
    }
  }
}