import { NgModule }      from '@angular/core';
import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { AppComponent }  from './app.component';
import { HomeComponent }  from './home.component';
import { HeaderComponent } from './header.component';
import { footer_oneComponent } from './footer_one.component';
import { footer_twoComponent } from './footer_two.component';
import { PortFolioComponent }  from './portfolio.component';
import { AboutUsComponent }  from './about-us.component';
import { ContactUsComponent }  from './contact-us.component';
import { notFoundComponent }  from './notFound.component';
import { blogComponent }  from './blog.component';
import { blogsingleComponent }  from './blog-single.component';
import { brandingComponent }  from './branding.component';
import { careerComponent }  from './career.component';
import { mobiledevelopmentComponent }  from './mobile-development.component';
import { promotionalComponent }  from './promotional.component';
import { servicesComponent }  from './services.component';
import { startprojectComponent }  from './start-project.component';
import { webdevelopmentComponent }  from './web-development.component';
import { worksingleaaraComponent }  from './work-single-aara.component';
import { PristinitRoutingModule } from './pristinit-routing.module';
import { PortfolioGetService }  from './portfolio.get.service';
import { MouseWheelDirective } from './mousewheel.directive';

@NgModule({
  imports:      [ BrowserModule, PristinitRoutingModule, HttpModule, BrowserAnimationsModule],
  declarations: [ AppComponent,notFoundComponent, HomeComponent,HeaderComponent,footer_oneComponent,footer_twoComponent, PortFolioComponent, AboutUsComponent, ContactUsComponent,blogComponent, blogsingleComponent, brandingComponent, careerComponent, mobiledevelopmentComponent, promotionalComponent, servicesComponent, startprojectComponent, webdevelopmentComponent, worksingleaaraComponent, MouseWheelDirective],
  providers: [ PortfolioGetService ],
  bootstrap:    [ AppComponent ]
  
})
export class PristinitModule { }
