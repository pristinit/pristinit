import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { PristinitModule } from './app/pristinit.module';

platformBrowserDynamic().bootstrapModule(PristinitModule);


